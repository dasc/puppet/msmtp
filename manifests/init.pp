# The msmtp class
#
# @summary installs and configures the msmtp mail transport agent
#
# @example Basic usage
#   class { '::msmtp':
#       mailhub => '10.0.0.1',
#   }
#
# @param mailhub
#   The hostname or IP address of the local unauthenticated mail relay
#
# @param hostname
#   The full hostname of "this" system
#
# @param allow_from_override
#   Control whether From: line modification is allowed by the submitting user
#
class msmtp (
    String            $mailhub,
    Optional[String]  $from     = undef,
) {
    package { 'msmtp': ensure => installed, }

    package { 'pcp-pmda-postfix': ensure => absent, }
    -> package { 'postfix-perl-scripts': ensure => absent, }
    -> package { 'postfix': ensure => absent, }

    package { 'exim4': ensure => absent, }

    file { '/etc/msmtprc':
        ensure  => file,
        owner   => 'root',
        group   => 'mail',
        mode    => '0644',
        content => epp('msmtp/etc/msmtprc.epp', {
            'mailhub' => $mailhub,
            'from'    => $from,
        }),
    }
}
